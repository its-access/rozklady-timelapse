import React, { Component } from 'react';
import VehicleMap from './VehicleMap';
import Selection from './Selection';
import Summary from './Summary';

import _ from 'lodash';

function getTimestamps(data) {
    return data.reduce((prev, elem) => {
        if (!prev.includes(elem.timestamp)) {
            return [...prev, elem.timestamp];
        }
        return prev;
    }, []).sort();
}

class App extends Component {
    constructor(props) {
        super(props);
        this.nextFrame = this.nextFrame.bind(this);
        this.play = this.play.bind(this);
        this.stop = this.stop.bind(this);
        this.timer = null;
        this.state = { data: [], timestamps: [], currentTimestamp: -1, currentData: [], frames: {} };
    }
    componentDidMount() {
        this.serverRequest = fetch(this.props.dataSource)
            .then(response => response.json())
            .then(vehicles => {
                const timestamps = getTimestamps(vehicles);
                const frames = _.groupBy(vehicles, 'timestamp');
                this.setState({
                    data: vehicles,
                    timestamps,
                    frames,
                    currentTimestamp: 0,
                    currentData: frames[timestamps[0]],
                });
            });
    }

    componentWillUnmount() {
        this.serverRequest.abort();
    }
    nextFrame() {
        if (this.state.currentTimestamp === this.state.timestamps.length - 1) {
            this.setState({
                currentTimestamp: 0,
                currentData: this.state.frames[this.state.timestamps[0]],
            });
            return;
        }
        this.setState({
            currentTimestamp: this.state.currentTimestamp + 1,
            currentData: this.state.frames[this.state.timestamps[this.state.currentTimestamp + 1]],
        });
    }
    play() {
        if (this.timer !== null) {
            return;
        }
        this.timer = setInterval(this.nextFrame, 2000);
    }
    stop() {
        clearInterval(this.timer);
        this.timer = null;
    }
    render() {
        return (<div className = "main">
        <div className = "header">
            Timelapse z komunikacji miejskiej w Łodzi / taavit@ssc
            <button onClick={this.nextFrame}>NEXT -></button>
            <button onClick={this.play}>PLAY!</button>
            <button onClick={this.stop}>STOP!</button>
            <span>{this.state.timestamps[this.state.currentTimestamp]}</span>
        </div>
        <div className = "container">
            <Selection vehicles={this.state.currentData} />
            <VehicleMap vehicles = {this.state.currentData} />
            <Summary vehicles = { this.state.currentData } />
            </div>
        </div>);
    }
}

export default App;
