import React, { PropTypes } from 'react';
import VehicleRow from './VehicleRow';

const VehicleTable = (props) =>
    <table>
        <tbody>
            {props.vehicles.map(vehicle => <VehicleRow key={vehicle.number} {...vehicle} />)}
        </tbody>
    </table>;

VehicleTable.propTypes = {
    vehicles: PropTypes.array,
};

export default VehicleTable;
