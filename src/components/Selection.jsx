import React, { PropTypes, Component } from 'react';

import LineRow from './LineRow';

import _ from 'lodash';

function groupByLine(data) {
    const lines = _.groupBy(data, 'line');
    const result = [];
    Object.keys(lines).forEach((line) => {
        result.push({ line, vehicles: lines[line] });
    });
    return result;
}

class Selection extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: 'lines',
        };
        this.selectVehicles = this.selectVehicles.bind(this);
        this.selectLines = this.selectLines.bind(this);
    }
    selectLines(e) {
        if (!e.target.checked) {
            return;
        }
        this.setState({ selected: 'lines' });
    }

    selectVehicles(e) {
        if (!e.target.checked) {
            return;
        }
        this.setState({ selected: 'vehicles' });
    }

    render() {
        return (
            <div className="selection">
                <header>
                    <label>
                        <input
                            type="radio"
                            name="selection"
                            onChange={this.selectVehicles}
                            checked={this.state.selected === 'vehicles'}
                        ></input>Pojazdy
                    </label>
                    <label>
                        <input
                            type="radio"
                            name="selection"
                            onChange={this.selectLines}
                            checked={this.state.selected === 'lines'}
                        ></input>Linie
                    </label>
                </header>
                {this.state.selected === 'vehicles' ? <div>
                    {this.props.vehicles.map((elem) => (<div key={elem.radio_nb}>
                            <span><strong>{elem.line}</strong></span>/{elem.radio_nb}/<span>{elem.difference}</span>
                        </div>))}
                </div> : null }
                {this.state.selected === 'lines' ? <div>
                    {groupByLine(this.props.vehicles).map((elem) => <LineRow key={elem.line} {...elem} />)}
                </div> : null}
            </div>);
    }
}

export default Selection;
