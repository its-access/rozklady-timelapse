import React, { PropTypes } from 'react';
import moment from 'moment';

const VehicleRow = (props) =>
    <tr>
        <td>{ props.nr }</td>
        <td>{ moment.duration(props.difference, 'seconds').minutes() }</td>
        <td>{ props.type }</td>
    </tr>;

VehicleRow.propTypes = {
    nr: PropTypes.number,
    difference: PropTypes.number,
    type: PropTypes.string,
};

export default VehicleRow;
