import React from 'react';

const LineRow = (props) => (<div style = {{display: 'flex'}}>
    <div style={{flex: 1}}>{props.line}</div>
    <div style={{flex: 1}}>{props.vehicles.length}</div>
    <div style={{flex: 2}}>{props.vehicles.filter((vehicle) => vehicle.difference < 0).reduce((diff, vehicle) => diff + vehicle.difference, 0)}</div>
    <div style={{flex: 2}}>{props.vehicles.filter((vehicle) => vehicle.difference > 0).reduce((diff, vehicle) => diff + vehicle.difference, 0)}</div>
</div>);

export default LineRow;
