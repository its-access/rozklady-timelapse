import React, { PropTypes } from 'react';
// import Chart from './Chart';
import ReactHighcharts from 'react-highcharts';

function vehiclesTotalDelay(vehicles) {
    return vehicles
        .filter(vehicle => vehicle.difference < 0)
        .reduce((prev, vehicle) => prev + vehicle.difference, 0);
}

const Summary = (props) => <div className="statistics">
    <h1>Opóźnienie: {vehiclesTotalDelay(props.vehicles)}</h1>
    {/*<ReactHighcharts
        config={{ chart: {
            type: 'line',
        },
        title: {
            text: 'Fruit Consumption',
        },
        xAxis: {
            // categories: props.vehicles.map(vehicle => vehicle.radio),
        },
        yAxis: {
            title: {
                text: 'Opóźnienie',
            },
        },
        series: [
            {
                name: 'Suma',
                // data: props.vehicles.map(vehicle => vehicle.difference),
            },
            {
                name: 'Inne',
                // data: props.vehicles.map(vehicle => Math.sqrt(vehicle.difference)),
            },
        ],
    }}
    />*/}

</div>;

Summary.propTypes = {
    vehicles: PropTypes.array,
};

export default Summary;
