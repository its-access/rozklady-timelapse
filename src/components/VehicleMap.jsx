import React, { PropTypes } from 'react';
import { Map, Marker, Popup, TileLayer } from 'react-leaflet';

const VehicleMap = (props) => (
    <Map center={[51.7596, 19.4613]} zoom={13} className="map">
        <TileLayer
            url="http://{s}.tile.osm.org/{z}/{x}/{y}.png"
            attribution = {`
                &copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors,
                <a href="http://rozklady.lodz.pl">ITS - Zarząd Dróg i Transportu w Łodzi</a>`
            }
        />
    {props.vehicles.map((vehicle) => (
        <Marker key={vehicle.radio_nb} position={[parseFloat(vehicle.latitude), parseFloat(vehicle.longitude)]} />)
    )}
</Map>);

VehicleMap.propTypes = {
    vehicles: PropTypes.array,
};

export default VehicleMap;
