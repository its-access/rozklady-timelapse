import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';

// import 'mapbox.css';
import L from 'leaflet';

import './styles/main.scss';

const promise = Promise
    .all([
        fetch('/out1.json').then(response => response.json()),
        fetch('/out2.json').then(response => response.json()),
        fetch('/out3.json').then(response => response.json()),
        fetch('/out4.json').then(response => response.json()),
        fetch('/out5.json').then(response => response.json()),
    ])
    .then(values => [...values[0], ...values[1], ...values[2], ...values[3], ...values[4]])
    // .then((data) => console.log(data));
    .then(data => process(data))
    .then(([boxes, timestamps]) => init(boxes, timestamps));


    function prepareBoxes(ids, timestamps) {
        const boxes = {};
        for (let id of ids) {
            boxes[id] = [...timestamps].fill(null);
        }
        return boxes;
    }

    function fillBoxes(boxes, data, timestamps) {
        for (let current of data) {
            boxes[current.radio_nb][timestamps.indexOf(current.timestamp)] = current;
        }
        return boxes;
    }

function process(data) {
    data.sort((elem1, elem2) => elem1.timestamp > elem2.timestamp ? 1 : elem1.timestamp < elem2.timestamp ? -1 : 0);

    const timestamps = data.reduce((prev, current) => prev.includes(current.timestamp) ? prev : [...prev, current.timestamp], []);
    const ids = data.reduce((prev, current) => prev.includes(current.radio_nb) ? prev : [...prev, current.radio_nb], []);
    const lines = data.reduce((prev, current) => prev.includes(current.line) ? prev : [...prev, current.line], []);

    const boxes = prepareBoxes(ids, timestamps);
    fillBoxes(boxes, data, timestamps);
    return [boxes, timestamps, ids, lines];
}

function createMarkers(boxes, map) {
    const markers = {};
    for (let id of Object.keys(boxes)) {
        markers[id] = L.marker([
            boxes[id][0] !== null ? boxes[id][0].latitude : 0,
            boxes[id][0] !== null ? boxes[id][0].longitude : 0
        ]).addTo(map);
    }
    return markers;
}

function updateMarkers(markers, boxes, step) {
    for (let id of Object.keys(boxes)) {
        markers[id].setLatLng(boxes[id][step] !== null ? [boxes[id][step].latitude, boxes[id][step].longitude] : [0, 0]);
    }
}

function updateTimestamp(value) {
    document.getElementById('timestamp').textContent = value;
}

function init(boxes, timestamps) {
    const map = L.map('mapa').setView([51.7596, 19.4613], 13);
    const markers = createMarkers(boxes, map);

    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    function createTick(markers, boxes, initial, end, timestamps) {
        let step = initial;
        return () => {
            updateTimestamp(timestamp[step]);
            updateMarkers(markers, boxes, step);
            step += 1;
            if (step >= end) {
                step = 0;
            }
        }
    }

    setInterval(createTick(markers, boxes, 0, timestamps.length, timestamps), 500);
}




// ReactDOM.render(<App dataSource="/out.json" />, document.getElementById('react-app'));
